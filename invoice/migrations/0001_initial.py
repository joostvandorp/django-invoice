# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Currency'
        db.create_table('invoice_currency', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code', self.gf('django.db.models.fields.CharField')(unique=True, max_length=3)),
            ('pre_symbol', self.gf('django.db.models.fields.CharField')(blank=True, max_length=1)),
            ('post_symbol', self.gf('django.db.models.fields.CharField')(blank=True, max_length=1)),
        ))
        db.send_create_signal('invoice', ['Currency'])

        # Adding model 'Customer'
        db.create_table('invoice_customer', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=254)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=254)),
            ('website', self.gf('django.db.models.fields.CharField')(max_length=254)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=254)),
            ('address', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['addressbook.Address'], related_name='customer_set')),
            ('currency', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, to=orm['invoice.Currency'], null=True)),
        ))
        db.send_create_signal('invoice', ['Customer'])

        # Adding model 'Invoice'
        db.create_table('invoice_invoice', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('customer', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['invoice.Customer'])),
            ('invoice_id', self.gf('django.db.models.fields.CharField')(unique=True, blank=True, max_length=16)),
            ('invoice_date', self.gf('django.db.models.fields.DateField')(default=datetime.datetime(2014, 4, 25, 0, 0))),
            ('invoice_sent', self.gf('django.db.models.fields.DateField')(blank=True, null=True)),
            ('invoiced', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('paid_date', self.gf('django.db.models.fields.DateField')(blank=True, null=True)),
            ('of_type', self.gf('django.db.models.fields.IntegerField')(default=2, max_length=1)),
        ))
        db.send_create_signal('invoice', ['Invoice'])

        # Adding model 'InvoiceItem'
        db.create_table('invoice_invoiceitem', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('invoice', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['invoice.Invoice'], related_name='items')),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('unit_price', self.gf('django.db.models.fields.DecimalField')(decimal_places=2, max_digits=8)),
            ('quantity', self.gf('django.db.models.fields.DecimalField')(default=1, decimal_places=2, max_digits=8)),
        ))
        db.send_create_signal('invoice', ['InvoiceItem'])


    def backwards(self, orm):
        # Deleting model 'Currency'
        db.delete_table('invoice_currency')

        # Deleting model 'Customer'
        db.delete_table('invoice_customer')

        # Deleting model 'Invoice'
        db.delete_table('invoice_invoice')

        # Deleting model 'InvoiceItem'
        db.delete_table('invoice_invoiceitem')


    models = {
        'addressbook.address': {
            'Meta': {'object_name': 'Address', 'ordering': "['street_name']"},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'locality': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['addressbook.Locality']"}),
            'postal_code': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'street_name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '254'}),
            'street_number': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        'addressbook.country': {
            'Meta': {'object_name': 'Country', 'ordering': "['active', 'name']"},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iso2_code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '2'}),
            'iso3_code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '3'}),
            'iso_code_numeric': ('django.db.models.fields.PositiveSmallIntegerField', [], {'unique': 'True', 'max_length': '3'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'printable_name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        'addressbook.locality': {
            'Meta': {'object_name': 'Locality', 'ordering': "('name',)"},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '254'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['addressbook.Region']"})
        },
        'addressbook.region': {
            'Meta': {'object_name': 'Region', 'ordering': "('country', 'name')"},
            'abbrev': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '3'}),
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['addressbook.Country']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '254'})
        },
        'invoice.currency': {
            'Meta': {'object_name': 'Currency'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '3'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'post_symbol': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '1'}),
            'pre_symbol': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '1'})
        },
        'invoice.customer': {
            'Meta': {'object_name': 'Customer'},
            'address': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['addressbook.Address']", 'related_name': "'customer_set'"}),
            'currency': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'to': "orm['invoice.Currency']", 'null': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '254'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'website': ('django.db.models.fields.CharField', [], {'max_length': '254'})
        },
        'invoice.invoice': {
            'Meta': {'object_name': 'Invoice', 'ordering': "('-invoice_date', 'id')"},
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['invoice.Customer']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'invoice_date': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2014, 4, 25, 0, 0)'}),
            'invoice_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'blank': 'True', 'max_length': '16'}),
            'invoice_sent': ('django.db.models.fields.DateField', [], {'blank': 'True', 'null': 'True'}),
            'invoiced': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'of_type': ('django.db.models.fields.IntegerField', [], {'default': '2', 'max_length': '1'}),
            'paid_date': ('django.db.models.fields.DateField', [], {'blank': 'True', 'null': 'True'})
        },
        'invoice.invoiceitem': {
            'Meta': {'object_name': 'InvoiceItem'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'invoice': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['invoice.Invoice']", 'related_name': "'items'"}),
            'quantity': ('django.db.models.fields.DecimalField', [], {'default': '1', 'decimal_places': '2', 'max_digits': '8'}),
            'unit_price': ('django.db.models.fields.DecimalField', [], {'decimal_places': '2', 'max_digits': '8'})
        }
    }

    complete_apps = ['invoice']