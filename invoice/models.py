from datetime import date
from datetime import timedelta
from decimal import Decimal
from io import StringIO
from email.mime.application import MIMEApplication
from uuid import uuid1
from django.db import models
from django.conf import settings
from django.template.loader import render_to_string
from django.core.mail import EmailMessage
from django.utils.encoding import python_2_unicode_compatible
from addressbook.models import Address
from invoice.conf import settings as app_settings
from invoice.utils import format_currency
from invoice.pdf import draw_pdf


@python_2_unicode_compatible
class Currency(models.Model):
    code = models.CharField(unique=True, max_length=3)
    pre_symbol = models.CharField(blank=True, max_length=1)
    post_symbol = models.CharField(blank=True, max_length=1)

    def __str__(self):
        return self.code


@python_2_unicode_compatible
class Customer(models.Model):
    name = models.CharField(max_length=254)
    email = models.EmailField(max_length=254)
    website = models.CharField(max_length=254)
    phone = models.CharField(max_length=254)
    address = models.ForeignKey(Address, related_name='%(class)s_set')
    currency = models.ForeignKey(Currency, blank=True, null=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('invoice:customer_detail',
                       kwargs={'pk': self.id})

    def get_amount_outstanding(self):
        """
        Returns a dictionary of outstanding amounts.
        """
        outstanding = { "0 - 30 days": 0.0,
                        "30 - 60 days": 0.0,
                        "60 - 90 days": 0.0,
                        "90+ days": 0.0,
                        "total": 0.0, }

        d30 = date.today() + timedelta(-30)
        d60 = date.today() + timedelta(-60)
        d90 = date.today() + timedelta(-90)

        for invoice in self.invoice_set.filter(invoiced = True,
                                               paid_date__isnull = False):
            outstanding["total"] += invoice.total()
            if invoice.invoice_sent >= d30:
                outstanding["0 - 30 days"] += invoice.total()
            elif (invoice.invoice_sent <= d30 and
                invoice.invoice_sent > d60):
                outstanding["30 - 60 days"] += invoice.total()
            elif (invoice.invoice_sent <= d60 and
                invoice.invoice_sent > d90):
                outstanding["60 - 90 days"] += invoice.total()
            elif invoice.invoice_sent <= d90:
                outstanding["90+ days"] += invoice.total()

        return outstanding


@python_2_unicode_compatible
class Invoice(models.Model):

    QUOTE = 1
    INVOICE = 2

    INVOICE_TYPE = (
        (QUOTE, 'Quote'),
        (INVOICE, 'Invoice'),
    )

    customer = models.ForeignKey(Customer)
    invoice_id = models.CharField(unique=True, max_length=16,
                                  blank=True, editable=False)
    invoice_date = models.DateField(default=date.today())
    invoice_sent = models.DateField(blank=True, null=True, editable=False)
    invoiced = models.BooleanField(default=False)
    paid_date = models.DateField(blank=True, null=True)
    of_type = models.IntegerField(
        max_length=1,
        choices=INVOICE_TYPE,
        default=INVOICE)

    class Meta:
        ordering = ('-invoice_date', 'id')

    def __str__(self):
        if self.of_type == self.INVOICE:
            return 'INVOICE: %s (%s)' % (self.invoice_id, self.total_amount())
        else:
            return 'QUOTE: %s (%s)' % (self.invoice_id, self.total_amount())

    def save(self, *args, **kwargs):
        super(Invoice, self).save(*args, **kwargs)
        if not self.invoice_id:
            yyyymmdd = self.invoice_date.strftime('%Y%m%d')
            unique_id = str(uuid.uuid1())[:8]
            self.invoice_id = yyyymmdd + unique_id
            kwargs['force_insert'] = False
            super(Invoice, self).save(*args, **kwargs)

    def total_amount(self):
        return format_currency(self.total(), self.currency)

    def total(self):
        total = Decimal('0.00')
        for item in self.items.all():
            total = total + item.total()
        return total

    def file_name(self):
        return 'Invoice-%s.pdf' % self.invoice_id

    def send_invoice(self):
        pdf = StringIO()
        draw_pdf(pdf, self)
        pdf.seek(0)

        attachment = MIMEApplication(pdf.read())
        attachment.add_header("Content-Disposition", "attachment",
                              filename=self.file_name())
        pdf.close()

        subject = app_settings.INV_EMAIL_SUBJECT % {"invoice_id": self.invoice_id}
        email = EmailMessage(subject=subject, to=[self.customer.email])
        email.body = render_to_string("invoice/invoice_email.txt", {
            "invoice": self,
            "SITE_NAME": settings.SITE_NAME,
            "INV_CURRENCY": app_settings.INV_CURRENCY,
            "INV_CURRENCY_SYMBOL": app_settings.INV_CURRENCY_SYMBOL,
            "SUPPORT_EMAIL": settings.MANAGERS[0][1],
        })
        email.attach(attachment)
        email.send()

        if not self.invoice_sent:
            # we record the date when the invoice was sent.
            # subsequent emails must not alter the original sent date.
            self.invoice_sent = date.today()

        self.invoiced = True
        self.save()


@python_2_unicode_compatible
class InvoiceItem(models.Model):
    invoice = models.ForeignKey(Invoice, related_name='items', unique=False)
    description = models.CharField(max_length=100)
    unit_price = models.DecimalField(max_digits=8, decimal_places=2)
    quantity = models.DecimalField(max_digits=8, decimal_places=2, default=1)

    def total(self):
        total = Decimal(str(self.unit_price * self.quantity))
        return total.quantize(Decimal('0.01'))

    def __str__(self):
        return self.description
